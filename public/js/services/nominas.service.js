angular.module('appVisits')
    .service('paymentsService', paymentsService);
paymentsService.$inject = ['$resource' , 'APPI'];

function paymentsService($resource , APPI) {
    var path_server         = APPI.URL_BASE+"nominas/";
    //var urlBase = 'http://192.168.1.130:65432/api/visits/page/';

    var nominas_pagadas  = $resource( path_server + 'payments/semestre' , {} , {
            getTotalPayments: {
                method: 'GET',
                params: {}
            },
        }),
        salarios_totales = $resource( path_server + 'payments/top-ten' , {} , {
            getTotalSalarys: {
                method: 'GET',
                params: {}
            },
        }),
        teachers         = $resource( path_server + 'teachers/page/:page' , {} , {
            list: {
                method: 'GET',
                params: {
                    page: '@page'
                }
            },
        }),
        teacher          = $resource( path_server + 'teachers/:id' , {} , {
            getInfo: {
                method: 'GET',
                params: {
                    page: '@id'
                }
            },
        });

    return {
        total_payments: function( parametros , callback ) {
            //console.log(parametros);
            return nominas_pagadas.getTotalPayments( parametros , function( data ){
                callback( data );
            });
        },
        total_salarys: function( parametros , callback ) {
            //console.log(parametros);
            return salarios_totales.getTotalSalarys( parametros , function( data ){
                callback( data );
            });
        },
        list_teachers: function( parametros , callback ) {
            //console.log(parametros);
            return teachers.list( parametros , function( data ){
                callback( data );
            });
        },
        get_teacher: function( parametros , callback ) {
            //console.log(parametros);
            return teacher.getInfo( parametros , function( data ){
                callback( data );
            });
        },
    }
}