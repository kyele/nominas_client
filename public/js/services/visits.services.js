angular.module('appVisits')
    .service('invitationsService', invitationsService);
invitationsService.$inject = ['$resource' , 'APPI'];

function invitationsService($resource , APPI) {
    var path_server         = APPI.URL_BASE;
    var visitsAPI           = {};
    //var urlBase = 'http://192.168.1.130:65432/api/visits/page/';

    var invitaciones_resource   = $resource( path_server + 'visits/page/:page' , {} , {
            getInvitations: {
                method: 'GET',
                params: {
                    page: '@page'
                }
            },
        })
        visits                  = $resource( path_server + "visits/:id" , {} , {
            details: {
                method: "GET",
                params: {
                    id: "@id"
                }
            },
            create: {
                method: "POST"
            }
        })
        cancel_visit            = $resource( path_server + "visits/cancel/:id",{},{
            cancel: {
                method: "PUT",
                params:{
                    id:"@id"
                }
            }
        })
        visitor                 = $resource( path_server + "visits/visitor/:visit_id/:visit_has_visitor_id",{},{
            delete: {
                method: "DELETE",
                params:{
                    visit_id             : "@visit_id",
                    visit_has_visitor_id : "@visit_has_visitor_id"
                }
            }
        })
        employees               = $resource( path_server + 'employees/page/:page' , {} , {
            list: {
                method: 'GET',
                params: {
                    page: '@page'
                }
            },
        })
        departments             = $resource( path_server + 'departments/page/:page' , {} , {
            list: {
                method: 'GET',
                params: {
                    page: '@page'
                }
            },
        })
        locations               = $resource( path_server + 'locations/page/:page' , {} , {
            list: {
                method: 'GET',
                params: {
                    page: '@page'
                }
            },
        })
        companies               = $resource( path_server + 'companies/page/:page' , {} , {
            list: {
                method: 'GET',
                params: {
                    page: '@page'
                }
            },
        })
        company                 = $resource( path_server + 'companies' , {} , {
            create: {
                method: 'POST',
            },
        })
        visitant                = $resource( path_server + 'visitors' , {} , {
            create: {
                method: 'POST',
            },
        })
        visitors                = $resource( path_server + 'visitors/page/:page' , {} , {
            list: {
                method: 'GET',
                params: {
                    page: '@page'
                }
            },
        })
        visitor_cathegory       = $resource( path_server + 'cathegories/page/:page' , {} , {
            list: {
                method: 'GET',
                params: {
                    page: '@page'
                }
            },
        });

    return {
        listar: function( parametros , callback ) {
            //console.log(parametros);
            return invitaciones_resource.getInvitations( parametros , function( data ){
                callback( data );
            });
        },
        add_visit: function( visit , success , fail ) {
            return visits.create( visit , function(data) {
                    success(data);
                },
                function( data ){
                    fail(data.data);
                }
            );
        },
        details: function( parametros , callback ) {
            return visits.details( parametros , function( data ) {
                callback( data );
            });
        },
        cancelar: function( parametros , callback ) {
            return cancel_visit.cancel( parametros , function( data ){
                callback( data );
            });
        },
        delete_visitor: function( parametros , success, fail ) {
            return visitor.delete( parametros , function( data ) {
                    success( data );
                },
                function( data ) {
                    fail( data.data );
                }
            );
        },
        listar_anfitriones: function( parametros , callback ) {
            //console.log(parametros);
            return employees.list( parametros , function( data ){
                callback( data );
            });
        },
        listar_departamentos: function( parametros , callback ) {
            //console.log(parametros);
            return departments.list( parametros , function( data ){
                callback( data );
            });
        },
        listar_companias: function( parametros , callback ) {
            //console.log(parametros);
            return companies.list( parametros , function( data ){
                callback( data );
            });
        },
        listar_edificios: function( parametros , callback ) {
            //console.log(parametros);
            return locations.list( parametros , function( data ){
                callback( data );
            });
        },
        listar_visitantes: function( parametros , callback ) {
            //console.log(parametros);
            return visitors.list( parametros , function( data ){
                callback( data );
            });
        },
        add_visitor: function( visitor , success , fail ) {
            return visitant.create( visitor , function(data) {
                    success(data);
                },
                function( data ){
                    fail(data.data);
                }
            );
        },
        listar_categorias: function( parametros , callback ) {
            //console.log(parametros);
            return visitor_cathegory.list( parametros , function( data ){
                callback( data );
            });
        },
        add_company: function( company_data , success , fail ) {
            return company.create( company_data , function(data) {
                    success(data);
                },
                function( data ){
                    fail(data.data);
                }
            );
        },

    };
}