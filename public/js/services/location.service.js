angular.module('appVisits')
    .service('locationsService', locationsService);
locationsService.$inject = ['$resource' , 'APPI'];

function locationsService($resource , APPI) {
    var path_server         = APPI.URL_BASE;
    var visitsAPI           = {};
    //var urlBase = 'http://192.168.1.130:65432/api/visits/page/';

    var location            = $resource( path_server + "locations/:id" , {} , {
            details: {
                method: "GET",
                params: {
                    id: "@id"
                }
            },
            create: {
                method: "POST"
            },
            edit: {
                method: "PUT",
                params: {
                    id: "@id"
                }  
            }
        })
        locations            = $resource( path_server + 'locations/page/:page' , {} , {
            list: {
                method: 'GET',
                params: {
                    page: '@page'
                }
            },
        });

    return {
        listar: function( parametros , callback ) {
            //console.log(parametros);
            return locations.list( parametros , function( data ){
                callback( data );
            });
        },
        guardar: function( visit , success , fail ) {
            return location.create( visit , function(data) {
                    success(data);
                },
                function( data ){
                    fail(data.data);
                }
            );
        },
        editar: function( visit , success , fail ) {
            return location.edit( visit , function(data) {
                    success(data);
                },
                function( data ){
                    fail(data.data);
                }
            );
        },
        detalles: function( parametros , callback ) {
            return location.details( parametros , function( data ) {
                callback( data );
            });
        },
    };
}