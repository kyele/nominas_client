angular.module( 'appVisits' )
    .controller( 'paymentCtrl' , paymentCtrl );
paymentCtrl.$inject = [ '$scope' , '$rootScope' , 'paymentsService' , '$state'];
function paymentCtrl( $scope , $rootScope ,  paymentsService , $state ) {
    var payment      = this;
    payment.calendar = "2017A";
    payment.career   = "INCO";
    payment.payments_total = function(){
        console.log(payment.calendar,payment.career);
        paymentsService.total_payments( 
            {
                calendar : payment.calendar ? payment.calendar : "2017A",
                career   : payment.career ? payment.career : "INCO"
            },
            function(data){
                console.log(data);
                payment.data        = data;
            }
        );
    }

    payment.prueba = function(){
        console.log("hola pinchi putita");
    }

    payment.payments_total();
}

angular.module( 'appVisits' )
    .controller( 'topTenCtrl' , topTenCtrl );
topTenCtrl.$inject = [ '$scope' , '$rootScope' , 'paymentsService' , '$state'];
function topTenCtrl( $scope , $rootScope ,  paymentsService , $state ) {
    var salary      = this;
    salary.inco;
    salary.inni;
    salary.inbi;
    salary.ince;
    salary.calendar = "2017A";

    salary.top_ten_inco = function(){
        paymentsService.total_salarys( 
            {
                calendar : salary.calendar ? salary.calendar : "2017A",
                career   : "INCO"
            },
            function(data){
                console.log(data.collection);
                salary.inco = data.collection;
            }
        );
    }

    salary.top_ten_inni = function(){
        paymentsService.total_salarys( 
            {
                calendar : salary.calendar ? salary.calendar : "2017A",
                career   : "INNI"
            },
            function(data){
                console.log(data.collection);
                salary.inni = data.collection;
            }
        );
    }

    salary.top_ten_inbi = function(){
        paymentsService.total_salarys( 
            {
                calendar : salary.calendar ? salary.calendar : "2017A",
                career   : "INBI"
            },
            function(data){
                console.log(data.collection);
                salary.inbi = data.collection;
            }
        );
    }

    salary.top_ten_ince = function(){
        paymentsService.total_salarys( 
            {
                calendar : salary.calendar ? salary.calendar : "2017A",
                career   : "INCE"
            },
            function(data){
                console.log(data.collection);
                salary.ince = data.collection;
            }
        );
    }

    salary.all_top_ten = function(){
        salary.top_ten_inco();
        salary.top_ten_inni();
        salary.top_ten_inbi();
        salary.top_ten_ince();
    }

    salary.all_top_ten();
}

angular.module( 'appVisits' )
    .controller( 'employeeCtrl' , employeeCtrl );
employeeCtrl.$inject = [ '$scope' , '$rootScope' , 'paymentsService' , '$state'];
function employeeCtrl( $scope , $rootScope ,  paymentsService , $state ) {
    var employment      = this;
    employment.teachers = [];

    employment.employee_list  = function( id_page ){
        paymentsService.list_teachers( 
            {
                page:               id_page,
                filter:             employment.filter_visitor,
            },
            function(data){
                employment.teachers = data.collection;
                console.log(employment.teachers);
                if(employment.teachers.length == 0){
                    employment.teachers.message = "No se encontraron coincidencias";
                } else {
                    delete employment.teachers.message;
                }
            }
        );
    };

    employment.get_employee = function(employeeId){
        paymentsService.get_teacher( 
            {
                id: employeeId,
            },
            function(data){
                console.log(data);
                employment.data = data;
            }
        );
    }

    employment.add_employee   = function(empleado){
        employment.teacher = empleado.name + " " + empleado.last_name;
        employment.get_employee(empleado.id);
        employment.teachers   = [];
    };

    employment.destoy_list    = function(e) {
        var element = e.relatedTarget;
        if( (element != null) && ( element.classList.contains('list-group-item') || element.classList.contains('list-group') ) ) {
            e.preventDefault();
        } else {
            employment.teachers   = [];
        }
    };
}