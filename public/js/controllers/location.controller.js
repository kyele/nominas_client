angular.module( 'appVisits' )
    .controller( 'locationsCtrl' , locationsCtrl );
locationsCtrl.$inject = [ '$scope' , '$rootScope' , 'locationsService' , '$state' , '$uibModal' , 'privilegeFactory', 'MODULES' , '$auth'];
function locationsCtrl( $scope , $rootScope ,  locationsService , $state , $uibModal , privilegeFactory, MODULES , $auth ) {
    if(!privilegeFactory.hasPrivilege(MODULES.LOCATIONS, "&", $auth.getPayload().rights, "v")){
        swal(
          'Acceso denegado!',
          'Este usuario no tiene privilegios para este modulo',
          'warning'
        );
        $state.go('invitaciones');
    }
    var location          = this;
    location.page_size    = 10;
    location.filter       = '';
    location.params       = {};

    location.list         = function( id_page ){
        locationsService.listar( 
            {
                page:               id_page,
                page_size:          location.page_size,
                filter:             location.filter,
            },
            function(data){
                location.collection   = data.collection;
                location.totalItems   = data.pagination.rowCount;
                location.currentPage  = data.pagination.page;
                location.numPages     = data.pagination.pageCount;
                location.itemsPerPage = data.pagination.pageSize;
                location.maxSize      = 5;
                location.privilege_create = privilegeFactory.hasPrivilege(MODULES.LOCATIONS, "&", $auth.getPayload().rights, "c")
            }
        );
    }

    location.create       = function () {
        location.params.action   = 'crear';
        delete location.params.id;
        var modalInstance = $uibModal.open({
            animation      : location.animationsEnabled,
            ariaLabelledBy : 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl    : 'public/views/cms/modal_locations.html',
            size           : 'md',
            controller     : 'modalLocationsCtrl',
            controllerAs   : 'md_location',
            resolve        : {
                params : function() {
                    return location.params;
                }
            }
        });
        modalInstance.result.then(function(data) {
            console.log(data);
            location.list( 1 );
        },function(error){
            console.log(error);
        });
    }

    location.details      = function ( id ) {
        location.params.action = 'editar';
        location.params.id     = id;
        var modalInstance      = $uibModal.open({
            animation      : location.animationsEnabled,
            ariaLabelledBy : 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl    : 'public/views/cms/modal_locations.html',
            size           : 'md',
            controller     : 'modalLocationsCtrl',
            controllerAs   : 'md_location',
            resolve        : {
                params : function() {
                    return location.params;
                }
            }
        });
        modalInstance.result.then(function(data) {
            console.log(data);
            location.list( 1 );
        },function(error){
            console.log(error);
        });
    }

    location.list( 1 );
}

angular.module('appVisits')
    .controller( 'modalLocationsCtrl' , modalLocationsCtrl );
modalLocationsCtrl.$inject = [ '$uibModalInstance' , 'locationsService' , 'params' ,  'privilegeFactory', 'MODULES' , '$auth'  ];
function modalLocationsCtrl( $uibModalInstance ,  locationsService , params ,  privilegeFactory, MODULES , $auth  ){

    var md_location      = this;
    console.log(params);
    md_location.action   = params.action;

    if(params.id) {
        locationsService.detalles({id:params.id}, function(data){
            console.log(data);
            md_location.name        = data.name;
            md_location.description = data.description;
            md_location.privilege_edit = privilegeFactory.hasPrivilege(MODULES.LOCATIONS, "&", $auth.getPayload().rights, "u");
        })
    }

    md_location.save  = function(){
        locationsService.guardar( { name : md_location.name , description : md_location.description } , function(data){
            swal(
                'Hecho!',
                'Nuevo area de visita agregada.',
                'success'
            );
            console.log(data);
            md_location.close(data);
        }, function(data){
            console.log(data);
            swal("Algo ha salido mal!", "Parece que ocurrio algo inesperado, reportelo al area de sistemas", "warning");
        })
    }

    md_location.edit  = function(){
        locationsService.editar( { id: params.id , name : md_location.name , description : md_location.description } , function(data){
            swal(
                'Hecho!',
                'Are de visita editada.',
                'success'
            );
            md_location.close(data);
        }, function(data){
            console.log(data);
            swal("Algo ha salido mal!", "Parece que ocurrio algo inesperado, reportelo al area de sistemas", "warning");
        })
    }

    md_location.close = function (data) {        
        $uibModalInstance.close(data);
    };

    md_location.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}