
angular.module( 'appVisits' )
    .controller( 'invitationsListCtrl' , invitationsListCtrl );
invitationsListCtrl.$inject = [ '$scope' , '$rootScope' , 'invitationsService' , '$state' , '$uibModal' , 'privilegeFactory', 'MODULES', '$auth'];
function invitationsListCtrl( $scope , $rootScope ,  invitationsService , $state , $uibModal , privilegeFactory, MODULES , $auth) {
    //console.log(privilegeFactory.hasPrivilege(MODULES.VISITS, "&", $auth.getPayload().rights, "v"));
    var visits          = this;
    visits.page_size    = 10;
    visits.filter       = '';

    visits.list         = function( id_page ){
        invitationsService.listar( 
            {
                page:               id_page,
                page_size:          visits.page_size,
                filter:             visits.filter,
            },
            function(data){
                //console.log(data);
                visits.collection   = data.collection;
                visits.totalItems   = data.pagination.rowCount;
                visits.currentPage  = data.pagination.page;
                visits.numPages     = data.pagination.pageCount;
                visits.itemsPerPage = data.pagination.pageSize;
                visits.maxSize      = 5;
                visits.privilege_update = privilegeFactory.hasPrivilege(MODULES.VISITS, "&", $auth.getPayload().rights, "u");
            }
        );
    }

    visits.cancel_visit  = function( id_visit ){
        swal({
            title: 'Estas seguro?',
            text: "Esta visita sera cancelada y ya no podra activarse",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, cancelarla!'
        }).then(function () {
            invitationsService.cancelar( { id: id_visit } , function(data){
                visits.list( 1 );
                swal(
                    'Hecho!',
                    'Esta visita fue cancelada.',
                    'success'
                )
            });
        });
    }

    visits.details = function ( id , size , parentSelector) {
        var modalInstance = $uibModal.open({
            animation      : visits.animationsEnabled,
            ariaLabelledBy : 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl    : 'public/views/cms/modal_visit.html',
            size           : size,
            controller     : 'modalVisitDetailsCtrl',
            controllerAs   : 'md_visit',
            resolve        : {
                id_visit: function () {
                  return id;
                }
            }
        });
    }

    visits.list( 1 );
}

angular.module( 'appVisits' )
    .controller( 'invitationsCreateCtrl' , invitationsCreateCtrl )
invitationsCreateCtrl.$inject = [ '$scope', 'invitationsService' , '$http' , '$state' , '$uibModal' , '$location' , '$anchorScroll' , 'privilegeFactory', 'MODULES' , '$auth'];
function invitationsCreateCtrl( $scope , invitationsService , $http , $state , $uibModal , $location , $anchorScroll , privilegeFactory, MODULES , $auth) {
    if(!privilegeFactory.hasPrivilege(MODULES.VISITS, "&", $auth.getPayload().rights, "c")){
        swal(
          'Acceso denegado!',
          'Este usuario no tiene privilegios para este modulo',
          'warning'
        );
        $state.go('invitaciones');
    }
    var message_not_found        = "No se encontraron resultados con esta busqueda.";
    var myDate                   = new Date();
    var visit                    = this;
    visit.employees              = [];
    visit.departments            = [];
    visit.visitors               = [];
    visit.companies              = [];
    visit.locations              = [];
    visit.data                   = {};
    visit.visitors_data          = {};
    visit.locations_data         = {};
    visit.mesaage_snack          = "";
    visit.visitor_added          = false;
    visit.location_added         = false;
    visit.num_visitor            = 0;
    visit.data.expected_visitors = 0;
    visit.privilege_add_visitor = privilegeFactory.hasPrivilege(MODULES.VISITORS, "&", $auth.getPayload().rights, "c");
    visit.privilege_add_company = privilegeFactory.hasPrivilege(MODULES.COMPANIES, "&", $auth.getPayload().rights, "c")


    $('.datetimepicker-visit').datetimepicker({
        format: 'YYYY/MM/DD HH:mm',
        minDate: myDate,
    });

    $('.datetimepicker-visit2').datetimepicker({
        format: 'YYYY/MM/DD HH:mm',
    });

    $(".datetimepicker-visit").on("dp.change", function (e) {
        $('.datetimepicker-visit2').data("DateTimePicker").minDate(e.date);
        $("#form-date_start").removeClass('has-error').addClass('has-success');
        $("#form-date_end").removeClass('has-error').addClass('has-success');
    });

    visit.add_catalog = function ( catalog_type ) {
        var modalInstance = $uibModal.open({
            animation      : visit.animationsEnabled,
            ariaLabelledBy : 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl    : 'public/views/cms/modal_catalogs.html',
            size           : 'md',
            controller     : 'modalVisitCatalogsCtrl',
            controllerAs   : 'md_catalog',
            resolve        : {
                type: function () {
                  return catalog_type;
                }
            }
        });
    };

    visit.anfitrion_list = function( id_page ){
        invitationsService.listar_anfitriones(             {
                page:               id_page,
                filter:             visit.filter_employer,
            },
            function(data){
                visit.employees = data.collection;
                if(visit.employees.length == 0){
                    visit.employees.message = message_not_found;
                } else {
                    delete visit.employees.message;
                }
            }
        );
    };

    visit.department_list = function( id_page ){
        invitationsService.listar_departamentos( 
            {
                page:               id_page,
                filter:             visit.filter_department,
            },
            function(data){
                visit.departments = data.collection;
                if(visit.departments.length == 0){
                    visit.departments.message = message_not_found;
                } else {
                    delete visit.departments.message;
                }
            }
        );
    };

    visit.visitors_list  = function( id_page ){
        invitationsService.listar_visitantes( 
            {
                page:               id_page,
                filter:             visit.filter_visitor,
            },
            function(data){
                visit.visitors = data.collection;
                if(visit.visitors.length == 0){
                    visit.visitors.message = message_not_found;
                } else {
                    delete visit.visitors.message;
                }
            }
        );
    };

    visit.companies_list = function( id_page ){
        invitationsService.listar_companias( 
            {
                page:               id_page,
                filter:             visit.filter_company,
            },
            function(data){
                visit.companies = data.collection;
                if(visit.companies.length == 0){
                    visit.companies.message = message_not_found;
                } else {
                    delete visit.companies.message;
                }
            }
        );
    };

    visit.locations_list = function( id_page ){
        invitationsService.listar_edificios( 
            {
                page:               id_page,
                filter:             visit.filter_location,
            },
            function(data){
                visit.locations = data.collection;
                if(visit.locations.length == 0){
                    visit.locations.message = message_not_found;
                } else {
                    delete visit.locations.message;
                }
            }
        );
    };

    visit.set_employer   = function(employer){
        visit.employer                  = employer;
        visit.data.employee_identifier  = employer.RPE;
        visit.mesaage_snack             = "Anfitrión Agregado!";
        visit.toast();
        visit.employees                 = [];
        visit.filter_employer           = '';
        $("#form-employer").removeClass('has-error');
    };

    visit.set_company    = function( company ){
        visit.company             = company;
        visit.data.company_id     = company.id;
        visit.mesaage_snack       = "Empresa Agregada!";
        visit.toast();
        visit.companies           = [];
        visit.filter_company      = '';
        $("#form-company").removeClass('has-error');
    };

    visit.add_location    = function(location){
        //console.log(visit.locations_data);
        if( !visit.locations_data[location.id] ) {
            visit.mesaage_snack = "Visitante Agregado!";
            visit.toast();
            visit.locations_data[location.id] = location.name;
            visit.location_added = true;
        }
        visit.locations          = [];
        visit.filter_location    = '';
        $("#form-location").removeClass('has-error');
    };

    visit.add_visitor    = function(visitor){
        if( !visit.visitors_data[visitor.id] ) {
            visit.mesaage_snack = "Visitante Agregado!";
            visit.toast();
            visit.visitors_data[visitor.id] = visitor.name;
            visit.visitor_added = true;
            visit.data.expected_visitors ++;
            visit.num_visitor ++;
        }
        visit.visitors          = [];
        visit.filter_visitor    = '';
        $("#form-visitors").removeClass('has-error');
    };

    visit.destoy_list    = function(e) {
        var element = e.relatedTarget;
        if( (element != null) && ( element.classList.contains('list-group-item') || element.classList.contains('list-group') ) ) {
            e.preventDefault();
        } else {
            visit.employees   = [];
            visit.departments = [];
            visit.visitors    = [];
            visit.companies   = [];
            visit.locations   = [];
        }
    };

    visit.toast          = function() {
        $("#snackbar").addClass('show');
        setTimeout(function(){
            $("#snackbar").removeClass('show');
        }, 1500);
    };

    visit.set_liable     = function( visitor_id ){
        visit.data.visitor_id = parseInt(visitor_id);
        $("#form-titular").removeClass('error-form');
    };

    visit.delete_location = function( location_id ) {
        delete visit.locations_data[location_id];
        if( angular.equals(visit.locations_data, {}) ){
            visit.location_added = false;
            $("#form-location").addClass("has-error");
        }
    };

    visit.delete_visitor = function( visitor_id ) {
        delete visit.visitors_data[visitor_id];
        visit.data.expected_visitors --;
        visit. num_visitor --;
        if( angular.equals(visit.visitors_data, {}) ){
            visit.visitor_added   = false;
            $("#form-visitors").addClass("has-error");
        }
    };

    visit.load_template = function( visitor_id ) {
        var modalInstance = $uibModal.open({
            animation      : visit.animationsEnabled,
            ariaLabelledBy : 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl    : 'public/views/cms/modal_visit_template.html',
            size           : 'lg',
            controller     : 'modalTemplateVisitCtrl',
            controllerAs   : 'visits',
        });
        modalInstance.result.then(function ( id_visit ) {
            invitationsService.details( { id : id_visit } , function(data){
                visit.visitors_data             = {};
                visit.locations_data            = {};

                visit.data.issue                = data.issue;
                visit.data.visitor_id           = data.visitor_id;
                //visit.data.department_id        = data.department_id;
                visit.data.observations         = data.observations;
                visit.data.visit_end            = data.visit_end;
                visit.data.visit_start          = data.visit_start;
                visit.data.email                = data.email;
                visit.data.telephone            = data.telephone;
                visit.data.company_id           = data.company_id;
                visit.data.employee_identifier  = data.identifier;

                $('.datetimepicker-visit').val(data.visit_start);
                $('.datetimepicker-visit2').val(data.visit_end);

                visit.employer                  = { Name : data.employee };
                visit.department                = { name : data.department };
                visit.company                   = { name : data.company };
                visit.visitor_added             = true;
                visit.location_added            = true;

                angular.forEach(data.visitors , function(value , key){
                    visit.data.expected_visitors ++;
                    visit.visitors_data[value.visitor_id] = value.name;
                });

                angular.forEach(data.locations , function(value , key){
                    visit.locations_data[value.location_id] = value.name;
                });

                visit.num_visitor = visit.data.expected_visitors;
                visit.valid_form();
            });
        }, function (data) {
          //console.log(data);
        });
    };

    visit.addPost = function(){
        visit.data.visit_start = $('.datetimepicker-visit').val();
        visit.data.visit_end   = $('.datetimepicker-visit2').val();
        visit.data.visitors    = Object.keys(visit.visitors_data).map((element) => parseInt(element));
        visit.data.locations   = Object.keys(visit.locations_data).map((element) => parseInt(element));
        if(visit.valid_form()){
            invitationsService.add_visit( visit.data , function(data){
                    swal("Guardado!", "Se ha creado correctamente tu invitacion!", "success");
                    visit.data = {};
                    $state.go( 'invitaciones' );
                }, function(data){
                    console.log(data);
                    swal("Algo ha salido mal!", "Parece que ocurrio algo inesperado, reportelo al area de sistemas", "warning");
                }
            );            
        }
    };

    visit.valid_form = function() {
        var valid_form    = true;
        visit.errors_form = [];
        var d1 = $(".datetimepicker-visit").val();
        var d2 = $(".datetimepicker-visit2").val();

        if(typeof visit.data.issue === 'undefined') {
            valid_form = false;
            $("#form-issue").addClass('error-form');
            visit.errors_form.push({ ancla:"form-issue" , mensaje :'El asunto es requerido'});
        } else {
            $("#form-issue").removeClass('has-error').addClass('has-success');
        }
        if(typeof visit.data.employee_identifier === 'undefined'){
            valid_form = false;
            $("#form-employer").addClass('has-error');
            visit.errors_form.push({ ancla:"form-employer" , mensaje :'El anfitrion es requerido'});
        } else {
            $("#form-employer").removeClass('has-error').addClass('has-success');
        }
        if( angular.equals(visit.locations_data , {}) ){
            valid_form = false;
            $("#form-location").addClass('has-error');
            visit.errors_form.push({ ancla:"form-location" , mensaje :'Se requiere al menos un area de visita'});
        } else {
            $("#form-location").removeClass('has-error').addClass('has-success');
        }
        if( d1.length <= 0 ){
            valid_form = false;
            $("#form-date_start").addClass('has-error');
            visit.errors_form.push({ ancla:"form-date_start" , mensaje :'La fecha de inicio es requerida'});
        } else {
            $("#form-date_start").removeClass('has-error').addClass('has-success');
        }
        if( d2.length <= 0 ){
            valid_form = false;
            $("#form-date_end").addClass('has-error');
            visit.errors_form.push({ ancla:"form-date_end" , mensaje :'La fecha de fin es requerida'});
        } else {
            $("#form-date_end").removeClass('has-error').addClass('has-success');
        }
        if( angular.equals(visit.visitors_data , {}) ){
            valid_form = false;
            $("#form-visitors").addClass('has-error');
            visit.errors_form.push({ ancla:"form-visitors" , mensaje :'Se requiere al menos un visitante'});
        } else {
            $("#form-visitors").removeClass('has-error').addClass('has-success');
            if(typeof visit.data.visitor_id === 'undefined'){
                valid_form = false;
                $("#form-titular").addClass('error-form');
                visit.errors_form.push({ ancla:"form-titular" , mensaje :'Debe seleccionarse un titular para la visita'});
            } else {
                $("#form-titular").removeClass('has-error').addClass('has-success');
                if(typeof visit.data.email === 'undefined'){
                    valid_form = false;
                    $("#form-email").addClass('error-form');
                    visit.errors_form.push({ ancla:"form-email" , mensaje :'Se requiere el email del titular'});
                } else {
                    $("#form-email").removeClass('error-form').addClass('has-success');
                }
                if(typeof visit.data.telephone === 'undefined'){
                    valid_form = false;
                    $("#form-telephone").addClass('error-form');
                    visit.errors_form.push({ ancla:"form-telephone" , mensaje :'Se requiere el telefono del titular'});
                } else {
                    $("#form-telephone").removeClass('error-form').addClass('has-success');
                }
            }
        }
        if(typeof visit.data.company_id === 'undefined'){
            valid_form = false;
            $("#form-company").addClass('has-error');
            visit.errors_form.push({ ancla:"form-company" , mensaje :'La empresa del visitante es requerida'});
        } else {
            $("#form-company").removeClass('has-error').addClass('has-success');
        }
        $location.hash('registro');
        $anchorScroll();
        if(valid_form){
            return true;
        }
        return false;
    }

    visit.goAnchor = function(anchor) {
        $location.hash(anchor);
        $anchorScroll();
    }
};


angular.module('appVisits')
    .controller( 'modalVisitDetailsCtrl' , modalVisitDetailsCtrl );
modalVisitDetailsCtrl.$inject = [ '$uibModalInstance' , 'id_visit' , 'invitationsService' , 'privilegeFactory', 'MODULES' , '$auth' ];
function modalVisitDetailsCtrl( $uibModalInstance , id_visit , invitationsService , privilegeFactory, MODULES , $auth ){
    var md_visit = this;

    invitationsService.details( {id:id_visit} , function(data){
        md_visit.data = data;
        console.log(privilegeFactory.hasPrivilege(MODULES.VISITS, "&", $auth.getPayload().rights, "d"));
        md_visit.data.privilege_delete = privilegeFactory.hasPrivilege(MODULES.VISITS, "&", $auth.getPayload().rights, "d");
        console.log(md_visit.data);
    });

    md_visit.delete_visitor  = function( visit_has_visitor_id ){
        swal({
            title: 'Estas seguro?',
            text: "Este visitante sera eliminado de esta visita permanentemente",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminarlo!'
        }).then(function () {
            invitationsService.delete_visitor( { visit_id : id_visit , visit_has_visitor_id: visit_has_visitor_id } ,
                function(data){                    
                    invitationsService.details( {id:id_visit} , function(data){
                        md_visit.data = data;
                        swal(
                            'Hecho!',
                            'Esta visitante fue eliminado.',
                            'success'
                        )
                    });
                }, function(data){
                    var mensaje = (data.label !== 'CHECKIN_VISITOR') ? "No se puede eliminar al titular de la visita o a un visitante que ya ingreso" : "No se puede eliminar un visitante si la visita ha sido finalizada";
                    swal(
                        'Denegado!',
                        mensaje,
                        'error'
                    )
                });
        });
    }
    md_visit.ok = function () {
        $uibModalInstance.close();
    };

    md_visit.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
};


angular.module('appVisits')
    .controller( 'modalVisitCatalogsCtrl' , modalVisitCatalogsCtrl );
modalVisitCatalogsCtrl.$inject = [ '$uibModalInstance' , 'invitationsService' , 'type' ];
function modalVisitCatalogsCtrl( $uibModalInstance ,  invitationsService , type ){
    var md_catalog      = this;
    md_catalog.catalog  = type;
    md_catalog.types    = {};

    invitationsService.listar_categorias( 
        {
            page:               1,
            page_size:          100,
            filter:             "",
        },
        function(data){
            md_catalog.types = data.collection;
        }
    );
    md_catalog.save_catalog  = function(){
        //console.log(md_catalog.name);
        if( type == 'visitante'){
            invitationsService.add_visitor( { name : md_catalog.name , cathegory_id : md_catalog.type } , function(data){
                swal(
                    'Hecho!',
                    'Nuevo visitante agregado.',
                    'success'
                );
                md_catalog.name = "";
            }, function(data){

                swal("Algo ha salido mal!", "Parece que ocurrio algo inesperado, reportelo al area de sistemas", "warning");
            })
        } else if( type == 'empresa' ) {
            invitationsService.add_company( { name : md_catalog.name } , function(data){
                swal(
                    'Hecho!',
                    'Nuevo empresa agregada.',
                    'success'
                );
                md_catalog.name = "";
            }, function(data){
                swal("Algo ha salido mal!", "Parece que ocurrio algo inesperado, reportelo al area de sistemas", "warning");
            })
        }
    }
    md_catalog.close = function () {
        $uibModalInstance.close();
    };

    md_catalog.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}


angular.module( 'appVisits' )
    .controller( 'modalTemplateVisitCtrl' , modalTemplateVisitCtrl );
modalTemplateVisitCtrl.$inject = [ '$scope' , '$rootScope' , 'invitationsService' , '$state' , '$uibModal' , '$uibModalInstance' ];
function modalTemplateVisitCtrl( $scope , $rootScope ,  invitationsService , $state , $uibModal , $uibModalInstance ) {

    var visits          = this;
    visits.page_size    = 10;
    visits.filter       = '';

    visits.list         = function( id_page ){
        invitationsService.listar( 
            {
                page:               id_page,
                page_size:          visits.page_size,
                filter:             visits.filter,
            },
            function(data){
                //console.log(data);
                visits.collection   = data.collection;
                visits.totalItems   = data.pagination.rowCount;
                visits.currentPage  = data.pagination.page;
                visits.numPages     = data.pagination.pageCount;
                visits.itemsPerPage = data.pagination.pageSize;
                visits.maxSize      = 5;
            }
        );
    }

    visits.set_template = function( id_visit ) {
        //$rootScope.id_visit_preload =  id_visit;
        $uibModalInstance.close(id_visit);
    }

    visits.list( 1 );
}