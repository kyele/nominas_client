angular.module('appVisits', [ 'satellizer' , 'ui.router' , 'ngResource' , 'ngAnimate' , 'ngTouch' , 'ui.bootstrap' ])
    .constant('APPI' , {
        URL_BASE: "http://localhost:8084/api/"
    })
    .config(['$stateProvider', '$urlRouterProvider', 'APPI' ,
        function( $stateProvider, $urlRouterProvider, APPI ) {

            'use strict';

            $urlRouterProvider
                .when('/', '/system')
                .otherwise('/system');
            $stateProvider
                .state('system' , {
                    url:            '/system',
                    templateUrl:    'public/views/cms.html'
                })
                    .state('payments' , {
                        parent:         'system',
                        url:            '/pagos_por_semestre',
                        templateUrl:    'public/views/cms/payments.html',
                        controller:     'paymentCtrl',
                        controllerAs:   'payment'
                    })
                    .state('top-ten' , {
                        parent:         'system',
                        url:            '/trabajadores_mejor_pagados',
                        templateUrl:    'public/views/cms/top_ten.html',
                        controller:     'topTenCtrl',
                        controllerAs:   'salary'
                    })
                    .state('jobs' , {
                        parent:         'system',
                        url:            '/puestos_de_trabajo',
                        templateUrl:    'public/views/cms/jobs.html',
                        controller:     'employeeCtrl',
                        controllerAs:   'employment'
                    })
                .state("logout", {
                    url: "/logout",
                    templateUrl: null,
                    controller: "LogoutController"
                });
            //$locationProvider.html5Mode(true);
        }
    ]);
    /*
    .run([
        '$rootScope' , '$location' , '$auth' , '$state' , 
        function( $rootScope , $location , $auth , $state ){
            'use strict';
            var token            = $auth.getToken();
            var dateCreateToken  = (token != null) ? new Date((($auth.getPayload().iat)*1000)-360000) : new Date();
            var dateExpiredToken = (token != null) ? new Date(($auth.getPayload().exp)*1000) : new Date();
            var dateActual       = new Date();
            console.log($auth.getPayload());
            $rootScope.$state   = $state;
            $rootScope.$on( '$stateChangeStart' , function( event, toState, toParams, from, fromParams ){
                token                = $auth.getToken();
                var dateCreateToken  = (token != null) ? new Date((($auth.getPayload().iat)*1000)-360000) : new Date();
                var dateExpiredToken = (token != null) ? new Date(($auth.getPayload().exp)*1000) : new Date();
                dateActual           = new Date();
                if(!dates.inRange(dateActual , dateCreateToken , dateExpiredToken)){
                    token = null;
                }
                if( toState.name != 'login' && token == null){
                    event.preventDefault();
                    $state.go('login');
                } else if(toState.name != 'login' && token != null){
                } else if( toState.name == 'login' && token != null ) {
                    event.preventDefault();
                    if( !from.name ){
                        $state.go('invitaciones');
                    }
                }
            });
            var dates = {
                convert:function(d) {
                    return (
                        d.constructor === Date ? d :
                        d.constructor === Array ? new Date(d[0],d[1],d[2]) :
                        d.constructor === Number ? new Date(d) :
                        d.constructor === String ? new Date(d) :
                        typeof d === "object" ? new Date(d.year,d.month,d.date) :
                        NaN
                    );
                },
                inRange:function(d,start,end) {
                   return (
                        isFinite(d=this.convert(d).valueOf()) &&
                        isFinite(start=this.convert(start).valueOf()) &&
                        isFinite(end=this.convert(end).valueOf()) ?
                        start <= d && d <= end :
                        NaN
                    );
                }                
            }
        }
    ]);*/
